<?php

namespace Drupal\node_co_authors\Plugin\views\filter;

use Drupal\Core\Entity\Sql\TableMappingInterface;
use Drupal\user\Plugin\views\filter\Name;
use Drupal\views\Plugin\views\query\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A views filter for the name of the author or one of the co-authors.
 *
 * @ViewsFilter("author_co_author_name")
 */
class AuthorCoAuthorName extends Name {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple() {
    assert($this->query instanceof Sql);

    if (empty($this->value)) {
      return;
    }
    $this->ensureMyTable();

    $mapping = $this->entityTypeManager
      ->getStorage('node')
      ->getTableMapping();
    assert($mapping instanceof TableMappingInterface);

    $coAuthorsTable = $mapping->getFieldTableName('co_authors');
    $coAuthorsColumn = $mapping->getColumnNames('co_authors')['target_id'];

    $join = $this->query->getJoinData($coAuthorsTable, $this->tableAlias);
    $this->query->addRelationship($coAuthorsTable, $join, $this->tableAlias);

    // We use array_values() because the checkboxes keep keys and that can cause
    // array addition problems.
    $groupId = $this->query->setWhereGroup('OR');
    $this->query->addWhere($groupId, "$this->tableAlias.$this->realField", array_values($this->value), $this->operator);
    $this->query->addWhere($groupId, "$coAuthorsTable.$coAuthorsColumn", array_values($this->value), $this->operator);
  }

}
